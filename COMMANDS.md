## Available commands

### Application

|             Shortcut |                                        Description |                                 Command |
|----------------------|----------------------------------------------------|-----------------------------------------|
|          Alt-Shift-r |                                Random 3D rotations |          CMD_APP_RANDOMIZE_3D_ROTATIONS |
|                Alt-b |                                   Next 3D boundary |                CMD_APP_NEXT_3D_BOUNDARY |
|                Alt-r |                                Toggle 3D rotations |             CMD_APP_TOGGLE_3D_ROTATIONS |
|                Alt-c |                           Display current colormap |                CMD_APP_DISPLAY_COLORMAP |
|               Ctrl-f |                          Toggle full-screen on/off |               CMD_APP_SWITCH_FULLSCREEN |
|                Alt-m |                             Show/hide mouse cursor |                   CMD_APP_SWITCH_CURSOR |
|              Shift-q |                                               Quit |                            CMD_APP_QUIT |
|              Shift-x |                Save the current sequence then quit |                       CMD_APP_SAVE_QUIT |
|              Shift-n |              Fill current frame with random pixels |                CMD_APP_RANDOMIZE_SCREEN |
|                    n |                            Clear the current frame |                    CMD_APP_CLEAR_SCREEN |
|               ESCAPE |                          Turn off all auto changes |                 CMD_APP_STOP_AUTO_MODES |
|               RETURN |                      Toggle selected plugin on/off |          CMD_APP_TOGGLE_SELECTED_PLUGIN |
|           Shift-LEFT |                              Use previous sequence |               CMD_APP_PREVIOUS_SEQUENCE |
|          Shift-RIGHT |                                  Use next sequence |                   CMD_APP_NEXT_SEQUENCE |
|    Shift-PRINTSCREEN |                                  Take a screenshot |                      CMD_APP_SCREENSHOT |
|       Ctrl-BACKSPACE |                Make a sequence from system schemes |                   CMD_APP_RANDOM_SCHEME |
|            BACKSPACE |                      Select a random user sequence |                 CMD_APP_RANDOM_SEQUENCE |
|                    m |                                   Next random mode |                CMD_APP_NEXT_RANDOM_MODE |
|              Shift-m |                               Previous random mode |            CMD_APP_PREVIOUS_RANDOM_MODE |
|               Ctrl-t |                              Auto colormaps on/off |           CMD_APP_TOGGLE_AUTO_COLORMAPS |
|               Ctrl-i |                                 Auto images on/off |              CMD_APP_TOGGLE_AUTO_IMAGES |
|                SPACE |                              Bypass webcams on/off |                   CMD_APP_SWITCH_BYPASS |
|           Ctrl-SPACE |                         Set webcam reference image |            CMD_APP_SET_WEBCAM_REFERENCE |
|                  TAB |                                 Select next webcam |                     CMD_APP_NEXT_WEBCAM |
|               Ctrl-l |                               Lock selected plugin |            CMD_APP_LOCK_SELECTED_PLUGIN |
|             Ctrl-TAB |                                Freeze input on/off |                    CMD_APP_FREEZE_INPUT |
|              Shift-v |                                    Scale volume up |                 CMD_APP_VOLUME_SCALE_UP |
|                Alt-v |                                  Scale volume down |               CMD_APP_VOLUME_SCALE_DOWN |
|      Ctrl-Shift-LEFT |                                 Use first sequence |                  CMD_APP_FIRST_SEQUENCE |
|     Ctrl-Shift-RIGHT |                                  Use last sequence |                   CMD_APP_LAST_SEQUENCE |
|         Shift-PAGEUP |                                   Increase 3D zoom |                    CMD_APP_DEC_3D_SCALE |
|       Shift-PAGEDOWN |                                   Decrease 3D zoom |                    CMD_APP_INC_3D_SCALE |

### Plugins

|             Shortcut |                                        Description |                                 Command |
|----------------------|----------------------------------------------------|-----------------------------------------|
|                   UP |                             Select previous plugin |                        CMD_PLG_PREVIOUS |
|                 DOWN |                                 Select next plugin |                            CMD_PLG_NEXT |
|               PAGEUP |                      Scroll up in the plugins list |                       CMD_PLG_SCROLL_UP |
|             PAGEDOWN |                    Scroll down in the plugins list |                     CMD_PLG_SCROLL_DOWN |

### Sequences

|             Shortcut |                                        Description |                                 Command |
|----------------------|----------------------------------------------------|-----------------------------------------|
|              Shift-z |                         Reset the current sequence |                           CMD_SEQ_RESET |
|              Shift-l |            Toggle selected plugin as a lens on/off |                     CMD_SEQ_TOGGLE_LENS |
|             Shift-UP |             Select previous plugin in the sequence |                 CMD_SEQ_SELECT_PREVIOUS |
|           Shift-DOWN |                 Select next plugin in the sequence |                     CMD_SEQ_SELECT_NEXT |
|              Ctrl-UP |            Move selected plugin up in the sequence |                         CMD_SEQ_MOVE_UP |
|            Ctrl-DOWN |          Move selected plugin down in the sequence |                       CMD_SEQ_MOVE_DOWN |
|                Alt-y |   Select default layer mode for the current plugin |                   CMD_SEQ_LAYER_DEFAULT |
|              Shift-y |                             Select next layer mode |                      CMD_SEQ_LAYER_NEXT |
|               Ctrl-s |                Save current sequence as new (full) |                       CMD_SEQ_SAVE_FULL |
|               Ctrl-u |                       Update current full sequence |                     CMD_SEQ_UPDATE_FULL |
|         Ctrl-Shift-s |                Save current sequence as new (bare) |                       CMD_SEQ_SAVE_BARE |
|         Ctrl-Shift-u |                       Update current bare sequence |                     CMD_SEQ_UPDATE_BARE |
|               Alt-UP |                   Select previous plugin parameter |                  CMD_SEQ_PARAM_PREVIOUS |
|             Alt-DOWN |                       Select next plugin parameter |                      CMD_SEQ_PARAM_NEXT |
|             Alt-LEFT |                    Decrease plugin parameter value |                       CMD_SEQ_PARAM_DEC |
|            Alt-RIGHT |                    Increase plugin parameter value |                       CMD_SEQ_PARAM_INC |
|       Alt-Shift-LEFT |            Decrease plugin parameter value quickly |                  CMD_SEQ_PARAM_DEC_FAST |
|      Alt-Shift-RIGHT |            Increase plugin parameter value quickly |                  CMD_SEQ_PARAM_INC_FAST |

### Colormaps

|             Shortcut |                                        Description |                                 Command |
|----------------------|----------------------------------------------------|-----------------------------------------|
|                    e |                           Select previous colormap |                        CMD_COL_PREVIOUS |
|                    r |                               Select next colormap |                            CMD_COL_NEXT |
|                    t |                             Select random colormap |                          CMD_COL_RANDOM |

### Colormap shortcuts

|             Shortcut |                                        Description |                                 Command |
|----------------------|----------------------------------------------------|-----------------------------------------|
|              Shift-1 |                         Use colormap in shortcut 1 |                  CMD_COL_USE_SHORTCUT_1 |
|              Shift-2 |                         Use colormap in shortcut 2 |                  CMD_COL_USE_SHORTCUT_2 |
|              Shift-3 |                         Use colormap in shortcut 3 |                  CMD_COL_USE_SHORTCUT_3 |
|              Shift-4 |                         Use colormap in shortcut 4 |                  CMD_COL_USE_SHORTCUT_4 |
|              Shift-5 |                         Use colormap in shortcut 5 |                  CMD_COL_USE_SHORTCUT_5 |
|              Shift-6 |                         Use colormap in shortcut 6 |                  CMD_COL_USE_SHORTCUT_6 |
|              Shift-7 |                         Use colormap in shortcut 7 |                  CMD_COL_USE_SHORTCUT_7 |
|              Shift-8 |                         Use colormap in shortcut 8 |                  CMD_COL_USE_SHORTCUT_8 |
|              Shift-9 |                         Use colormap in shortcut 9 |                  CMD_COL_USE_SHORTCUT_9 |
|              Shift-0 |                        Use colormap in shortcut 10 |                 CMD_COL_USE_SHORTCUT_10 |
|         Ctrl-Shift-1 |              Assign current colormap to shortcut 1 |                CMD_COL_STORE_SHORTCUT_1 |
|         Ctrl-Shift-2 |              Assign current colormap to shortcut 2 |                CMD_COL_STORE_SHORTCUT_2 |
|         Ctrl-Shift-3 |              Assign current colormap to shortcut 3 |                CMD_COL_STORE_SHORTCUT_3 |
|         Ctrl-Shift-4 |              Assign current colormap to shortcut 4 |                CMD_COL_STORE_SHORTCUT_4 |
|         Ctrl-Shift-5 |              Assign current colormap to shortcut 5 |                CMD_COL_STORE_SHORTCUT_5 |
|         Ctrl-Shift-6 |              Assign current colormap to shortcut 6 |                CMD_COL_STORE_SHORTCUT_6 |
|         Ctrl-Shift-7 |              Assign current colormap to shortcut 7 |                CMD_COL_STORE_SHORTCUT_7 |
|         Ctrl-Shift-8 |              Assign current colormap to shortcut 8 |                CMD_COL_STORE_SHORTCUT_8 |
|         Ctrl-Shift-9 |              Assign current colormap to shortcut 9 |                CMD_COL_STORE_SHORTCUT_9 |
|         Ctrl-Shift-0 |             Assign current colormap to shortcut 10 |               CMD_COL_STORE_SHORTCUT_10 |

### Images

|             Shortcut |                                        Description |                                 Command |
|----------------------|----------------------------------------------------|-----------------------------------------|
|                    y |                              Select previous image |                        CMD_IMG_PREVIOUS |
|                    u |                                  Select next image |                            CMD_IMG_NEXT |
|                    i |                                Select random image |                          CMD_IMG_RANDOM |

### Image shortcuts

|             Shortcut |                                        Description |                                 Command |
|----------------------|----------------------------------------------------|-----------------------------------------|
|                Alt-1 |                            Use image in shortcut 1 |                  CMD_IMG_USE_SHORTCUT_1 |
|                Alt-2 |                            Use image in shortcut 2 |                  CMD_IMG_USE_SHORTCUT_2 |
|                Alt-3 |                            Use image in shortcut 3 |                  CMD_IMG_USE_SHORTCUT_3 |
|                Alt-4 |                            Use image in shortcut 4 |                  CMD_IMG_USE_SHORTCUT_4 |
|                Alt-5 |                            Use image in shortcut 5 |                  CMD_IMG_USE_SHORTCUT_5 |
|                Alt-6 |                            Use image in shortcut 6 |                  CMD_IMG_USE_SHORTCUT_6 |
|                Alt-7 |                            Use image in shortcut 7 |                  CMD_IMG_USE_SHORTCUT_7 |
|                Alt-8 |                            Use image in shortcut 8 |                  CMD_IMG_USE_SHORTCUT_8 |
|                Alt-9 |                            Use image in shortcut 9 |                  CMD_IMG_USE_SHORTCUT_9 |
|                Alt-0 |                           Use image in shortcut 10 |                 CMD_IMG_USE_SHORTCUT_10 |
|           Ctrl-Alt-1 |                 Assign current image to shortcut 1 |                CMD_IMG_STORE_SHORTCUT_1 |
|           Ctrl-Alt-2 |                 Assign current image to shortcut 2 |                CMD_IMG_STORE_SHORTCUT_2 |
|           Ctrl-Alt-3 |                 Assign current image to shortcut 3 |                CMD_IMG_STORE_SHORTCUT_3 |
|           Ctrl-Alt-4 |                 Assign current image to shortcut 4 |                CMD_IMG_STORE_SHORTCUT_4 |
|           Ctrl-Alt-5 |                 Assign current image to shortcut 5 |                CMD_IMG_STORE_SHORTCUT_5 |
|           Ctrl-Alt-6 |                 Assign current image to shortcut 6 |                CMD_IMG_STORE_SHORTCUT_6 |
|           Ctrl-Alt-7 |                 Assign current image to shortcut 7 |                CMD_IMG_STORE_SHORTCUT_7 |
|           Ctrl-Alt-8 |                 Assign current image to shortcut 8 |                CMD_IMG_STORE_SHORTCUT_8 |
|           Ctrl-Alt-9 |                 Assign current image to shortcut 9 |                CMD_IMG_STORE_SHORTCUT_9 |
|           Ctrl-Alt-0 |                Assign current image to shortcut 10 |               CMD_IMG_STORE_SHORTCUT_10 |

### Banks

|             Shortcut |                                        Description |                                 Command |
|----------------------|----------------------------------------------------|-----------------------------------------|
|        Ctrl-Shift-F1 |                                       Clear bank 1 |                    CMD_APP_CLEAR_BANK_1 |
|        Ctrl-Shift-F2 |                                       Clear bank 2 |                    CMD_APP_CLEAR_BANK_2 |
|        Ctrl-Shift-F3 |                                       Clear bank 3 |                    CMD_APP_CLEAR_BANK_3 |
|        Ctrl-Shift-F4 |                                       Clear bank 4 |                    CMD_APP_CLEAR_BANK_4 |
|        Ctrl-Shift-F5 |                                       Clear bank 5 |                    CMD_APP_CLEAR_BANK_5 |
|        Ctrl-Shift-F6 |                                       Clear bank 6 |                    CMD_APP_CLEAR_BANK_6 |
|        Ctrl-Shift-F7 |                                       Clear bank 7 |                    CMD_APP_CLEAR_BANK_7 |
|        Ctrl-Shift-F8 |                                       Clear bank 8 |                    CMD_APP_CLEAR_BANK_8 |
|        Ctrl-Shift-F9 |                                       Clear bank 9 |                    CMD_APP_CLEAR_BANK_9 |
|       Ctrl-Shift-F10 |                                      Clear bank 10 |                   CMD_APP_CLEAR_BANK_10 |
|       Ctrl-Shift-F11 |                                      Clear bank 11 |                   CMD_APP_CLEAR_BANK_11 |
|       Ctrl-Shift-F12 |                                      Clear bank 12 |                   CMD_APP_CLEAR_BANK_12 |
|       Ctrl-Shift-F13 |                                      Clear bank 13 |                   CMD_APP_CLEAR_BANK_13 |
|       Ctrl-Shift-F14 |                                      Clear bank 14 |                   CMD_APP_CLEAR_BANK_14 |
|       Ctrl-Shift-F15 |                                      Clear bank 15 |                   CMD_APP_CLEAR_BANK_15 |
|       Ctrl-Shift-F16 |                                      Clear bank 16 |                   CMD_APP_CLEAR_BANK_16 |
|       Ctrl-Shift-F17 |                                      Clear bank 17 |                   CMD_APP_CLEAR_BANK_17 |
|       Ctrl-Shift-F18 |                                      Clear bank 18 |                   CMD_APP_CLEAR_BANK_18 |
|       Ctrl-Shift-F19 |                                      Clear bank 19 |                   CMD_APP_CLEAR_BANK_19 |
|       Ctrl-Shift-F20 |                                      Clear bank 20 |                   CMD_APP_CLEAR_BANK_20 |
|       Ctrl-Shift-F21 |                                      Clear bank 21 |                   CMD_APP_CLEAR_BANK_21 |
|       Ctrl-Shift-F22 |                                      Clear bank 22 |                   CMD_APP_CLEAR_BANK_22 |
|       Ctrl-Shift-F23 |                                      Clear bank 23 |                   CMD_APP_CLEAR_BANK_23 |
|       Ctrl-Shift-F24 |                                      Clear bank 24 |                   CMD_APP_CLEAR_BANK_24 |
|             Shift-F1 |                  Assign current sequence to bank 1 |                    CMD_APP_STORE_BANK_1 |
|             Shift-F2 |                  Assign current sequence to bank 2 |                    CMD_APP_STORE_BANK_2 |
|             Shift-F3 |                  Assign current sequence to bank 3 |                    CMD_APP_STORE_BANK_3 |
|             Shift-F4 |                  Assign current sequence to bank 4 |                    CMD_APP_STORE_BANK_4 |
|             Shift-F5 |                  Assign current sequence to bank 5 |                    CMD_APP_STORE_BANK_5 |
|             Shift-F6 |                  Assign current sequence to bank 6 |                    CMD_APP_STORE_BANK_6 |
|             Shift-F7 |                  Assign current sequence to bank 7 |                    CMD_APP_STORE_BANK_7 |
|             Shift-F8 |                  Assign current sequence to bank 8 |                    CMD_APP_STORE_BANK_8 |
|             Shift-F9 |                  Assign current sequence to bank 9 |                    CMD_APP_STORE_BANK_9 |
|            Shift-F10 |                 Assign current sequence to bank 10 |                   CMD_APP_STORE_BANK_10 |
|            Shift-F11 |                 Assign current sequence to bank 11 |                   CMD_APP_STORE_BANK_11 |
|            Shift-F12 |                 Assign current sequence to bank 12 |                   CMD_APP_STORE_BANK_12 |
|         Alt-Shift-F1 |                             Use sequence in bank 1 |                      CMD_APP_USE_BANK_1 |
|         Alt-Shift-F2 |                             Use sequence in bank 2 |                      CMD_APP_USE_BANK_2 |
|         Alt-Shift-F3 |                             Use sequence in bank 3 |                      CMD_APP_USE_BANK_3 |
|         Alt-Shift-F4 |                             Use sequence in bank 4 |                      CMD_APP_USE_BANK_4 |
|         Alt-Shift-F5 |                             Use sequence in bank 5 |                      CMD_APP_USE_BANK_5 |
|         Alt-Shift-F6 |                             Use sequence in bank 6 |                      CMD_APP_USE_BANK_6 |
|         Alt-Shift-F7 |                             Use sequence in bank 7 |                      CMD_APP_USE_BANK_7 |
|         Alt-Shift-F8 |                             Use sequence in bank 8 |                      CMD_APP_USE_BANK_8 |
|         Alt-Shift-F9 |                             Use sequence in bank 9 |                      CMD_APP_USE_BANK_9 |
|        Alt-Shift-F10 |                            Use sequence in bank 10 |                     CMD_APP_USE_BANK_10 |
|        Alt-Shift-F11 |                            Use sequence in bank 11 |                     CMD_APP_USE_BANK_11 |
|        Alt-Shift-F12 |                            Use sequence in bank 12 |                     CMD_APP_USE_BANK_12 |
|        Alt-Shift-F13 |                            Use sequence in bank 13 |                     CMD_APP_USE_BANK_13 |
|        Alt-Shift-F14 |                            Use sequence in bank 14 |                     CMD_APP_USE_BANK_14 |
|        Alt-Shift-F15 |                            Use sequence in bank 15 |                     CMD_APP_USE_BANK_15 |
|        Alt-Shift-F16 |                            Use sequence in bank 16 |                     CMD_APP_USE_BANK_16 |
|        Alt-Shift-F17 |                            Use sequence in bank 17 |                     CMD_APP_USE_BANK_17 |
|        Alt-Shift-F18 |                            Use sequence in bank 18 |                     CMD_APP_USE_BANK_18 |
|        Alt-Shift-F19 |                            Use sequence in bank 19 |                     CMD_APP_USE_BANK_19 |
|        Alt-Shift-F20 |                            Use sequence in bank 20 |                     CMD_APP_USE_BANK_20 |
|        Alt-Shift-F21 |                            Use sequence in bank 21 |                     CMD_APP_USE_BANK_21 |
|        Alt-Shift-F22 |                            Use sequence in bank 22 |                     CMD_APP_USE_BANK_22 |
|        Alt-Shift-F23 |                            Use sequence in bank 23 |                     CMD_APP_USE_BANK_23 |
|        Alt-Shift-F24 |                            Use sequence in bank 24 |                     CMD_APP_USE_BANK_24 |
|    Ctrl-Alt-Shift-F1 |                                     Use bank set 1 |                  CMD_APP_USE_BANK_SET_1 |
|    Ctrl-Alt-Shift-F2 |                                     Use bank set 2 |                  CMD_APP_USE_BANK_SET_2 |
|    Ctrl-Alt-Shift-F3 |                                     Use bank set 3 |                  CMD_APP_USE_BANK_SET_3 |
|    Ctrl-Alt-Shift-F4 |                                     Use bank set 4 |                  CMD_APP_USE_BANK_SET_4 |
|    Ctrl-Alt-Shift-F5 |                                     Use bank set 5 |                  CMD_APP_USE_BANK_SET_5 |
|    Ctrl-Alt-Shift-F6 |                                     Use bank set 6 |                  CMD_APP_USE_BANK_SET_6 |
|    Ctrl-Alt-Shift-F7 |                                     Use bank set 7 |                  CMD_APP_USE_BANK_SET_7 |
|    Ctrl-Alt-Shift-F8 |                                     Use bank set 8 |                  CMD_APP_USE_BANK_SET_8 |
|    Ctrl-Alt-Shift-F9 |                                     Use bank set 9 |                  CMD_APP_USE_BANK_SET_9 |
|   Ctrl-Alt-Shift-F10 |                                    Use bank set 10 |                 CMD_APP_USE_BANK_SET_10 |
|   Ctrl-Alt-Shift-F11 |                                    Use bank set 11 |                 CMD_APP_USE_BANK_SET_11 |
|   Ctrl-Alt-Shift-F12 |                                    Use bank set 12 |                 CMD_APP_USE_BANK_SET_12 |
|   Ctrl-Alt-Shift-F13 |                                    Use bank set 13 |                 CMD_APP_USE_BANK_SET_13 |
|   Ctrl-Alt-Shift-F14 |                                    Use bank set 14 |                 CMD_APP_USE_BANK_SET_14 |
|   Ctrl-Alt-Shift-F15 |                                    Use bank set 15 |                 CMD_APP_USE_BANK_SET_15 |
|   Ctrl-Alt-Shift-F16 |                                    Use bank set 16 |                 CMD_APP_USE_BANK_SET_16 |
|   Ctrl-Alt-Shift-F17 |                                    Use bank set 17 |                 CMD_APP_USE_BANK_SET_17 |
|   Ctrl-Alt-Shift-F18 |                                    Use bank set 18 |                 CMD_APP_USE_BANK_SET_18 |
|   Ctrl-Alt-Shift-F19 |                                    Use bank set 19 |                 CMD_APP_USE_BANK_SET_19 |
|   Ctrl-Alt-Shift-F20 |                                    Use bank set 20 |                 CMD_APP_USE_BANK_SET_20 |
|   Ctrl-Alt-Shift-F21 |                                    Use bank set 21 |                 CMD_APP_USE_BANK_SET_21 |
|   Ctrl-Alt-Shift-F22 |                                    Use bank set 22 |                 CMD_APP_USE_BANK_SET_22 |
|   Ctrl-Alt-Shift-F23 |                                    Use bank set 23 |                 CMD_APP_USE_BANK_SET_23 |
|   Ctrl-Alt-Shift-F24 |                                    Use bank set 24 |                 CMD_APP_USE_BANK_SET_24 |
