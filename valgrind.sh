#!/usr/bin/env bash
#
# Make sure you compiled with ./configure --prefix=/usr --enable-debug --disable-dlclose
#
reset
export LD_LIBRARY_PATH=$PWD/src
export DEV_WEB_UI=1
valgrind --max-threads=3000 --tool=memcheck --leak-check=full --show-leak-kinds=all --track-origins=yes ./src/lebiniou -b plugins $* 2>&1 | tee valgrind.out
