#!/usr/bin/env bash
echo Validating .xml and .desktop files
appstream-util validate net.biniou.LeBiniou.appdata.xml && desktop-file-validate net.biniou.LeBiniou.desktop
