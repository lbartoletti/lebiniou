=head1	NAME

lebiniou - A tool to create images from sound

=head1 SYNOPSIS

B<lebiniou> [I<OPTIONS>]

=head1 DESCRIPTION

Le Biniou is a program which creates images from sound, in an intelligent way.

=head1 OPTIONS

B<-a | --audio> I<file>: Set audio input file (for the sndfile input plugin)

B<-b | --basedir> I<path>: Set base directory to look for plugins
(default: @prefix@/lib/lebiniou/plugins)

B<-c | --config> I<file>: Select the configuration file to use (default: S<~/.lebiniou/lebiniou.json>)

B<-d | --datadir> I<path>: Set data directory (default: S<@prefix@/share/lebiniou>)

B<-f | --fullscreen>: Set fullscreen

B<-h | --help>: Display this help and exit

B<-i | --input> I<plugin>: Set input plugin (inputs: @INPUT_PLUGINS@)

B<-m | --max-fps> I<fps>: Set maximum framerate

B<-n | --no-borders>: Disable window borders

B<-o | --output> I<plugin>: Set output plugin. (outputs: @OUTPUT_PLUGINS@)

B<-q | --quiet>: Only display important messages to console

B<-r | --random> I<mode>: Set auto-random mode

B<-s | --soft-timers>: Use soft timers instead of the system clock

B<-t | --themes> I<themes>: Comma-separated list of themes to use

B<-v | --version>: Display the version and exit

B<-w | --http-port> I<port>: Set HTTP API port

B<-x | --width> I<width>: Set width

B<-y | --height> I<height>: Set height

B<-z | --schemes> I<file>: Set schemes file (default: S<@prefix@/share/lebiniou/etc/schemes.json>)

B<-B | --no-browser>: Don't open the web interface in a browser

B<-C | --cam-flip> I<h|v>: Flip webcam image horizontally/vertically

B<-D | --device> I<device>: Webcam base (default: S</dev/video>)

B<-E | --start-encoding>: Start encoding directly

B<-F | --fade> I<delay>: Set fading delay (s)

B<-I | --instance> I<name>: Set the instance name

B<-N | --no-statistics>: Do not send usage statistics

B<-P | --playlist> I<file>: Set playlist file

B<-S | --stats>: Display statistics and exit

B<-W | --webcams> I<webcams>: Number of webcams (default: 1)

B<-X | --xpos> I<x position>: Set left position of the window

B<-Y | --ypos> I<y position>: Set top position of the window

=head1 CONTROLS

