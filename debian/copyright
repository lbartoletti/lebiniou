Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: lebiniou
Upstream-Contact: Olivier Girondel <olivier@biniou.info>
Source: https://biniou.net

Files: *
Copyright: 1994-2021 Olivier Girondel <olivier@biniou.info>
License: GPL-2+

Files: debian/*
Copyright: 2011-2021 Olivier Girondel <olivier@biniou.info>
License: GPL-2+

Files: net.biniou.LeBiniou.appdata.xml
Copyright: 2020-2021 Olivier Girondel <olivier@biniou.info>
License: MIT

Files: plugins/main/blur_gaussian/blur_gaussian.c
       plugins/main/broken_mirror/broken_mirror.c
       plugins/main/fadeout/fadeout.c
       plugins/main/monitor/monitor.c
       plugins/main/reflector/reflector.c
       plugins/main/spirals/spirals.c
Copyright: 1994-2021 Olivier Girondel <olivier@biniou.info>
           2014-2021 Frantz Balinski
License: GPL-2+

Files: plugins/input/oscaudio/oscaudio.c
Copyright: 1994-2021 Olivier Girondel <olivier@biniou.info>
           2021 Jérémie Astor
License: GPL-2+

Files: plugins/main/flow/flow.c
Copyright: 1994-2021 Olivier Girondel <olivier@biniou.info>
           1996 by Tim Auckland <tda10.geo@yahoo.com>
           2000 Stephen Davies
           1991 by Patrick J. Naughton
License: MIT-variant

Files: plugins/main/swarm/swarm.c
Copyright: 1994-2021 Olivier Girondel <olivier@biniou.info>
           1991 by Patrick J. Naughton
License: MIT-variant

Files: plugins/main/tv_fire/tv_fire.c
       plugins/main/tv_nervous/tv_nervous.c
       plugins/main/tv_predator/tv_predator.c
       plugins/main/tv_quark/tv_quark.c
       plugins/main/tv_streak/tv_streak.c
Copyright: 1994-2021 Olivier Girondel <olivier@biniou.info>
           2001-2006 FUKUCHI Kentaro
License: GPL-2+

Files: plugins/main/acid_drop/acid_drop.c
       plugins/main/image_beat_1/image_beat_1.c
       plugins/main/image_beat_2/image_beat_2.c
       plugins/main/image_beat_3/image_beat_3.c
       plugins/main/image_colrot_beat/image_colrot_beat.c
       plugins/main/image_colrot/image_colrot.c
       plugins/main/image_dissolve/image_dissolve.c
       plugins/main/image_drop/image_drop.c
       plugins/main/image_squares_beat/image_squares_beat.c
       plugins/main/paint_drops/paint_drops.c
       plugins/main/tv_colrot_beat/tv_colrot_beat.c
       plugins/main/tv_colrot_slow/tv_colrot_slow.c
       plugins/main/tv_colrot/tv_colrot.c
       plugins/main/tv_diff2/tv_diff2.c
       plugins/main/tv_diff3/tv_diff3.c
       plugins/main/tv_diff4/tv_diff4.c
       plugins/main/tv_diffbeat/tv_diffbeat.c
Copyright: 1994-2021 Olivier Girondel <olivier@biniou.info>
           2019-2021 Tavasti
License: GPL-2+

Files: plugins/main/include/blur.h
       plugins/main/include/freq.h
       plugins/main/include/gum.h
       plugins/main/include/oscillo.h
       plugins/main/include/path.h
       plugins/main/include/scroll.h
       plugins/main/path_freq/path_freq.c
       plugins/main/path_oscillo_freq/path_oscillo_freq.c
       plugins/main/path_oscillo/path_oscillo.c
       plugins/main/path/path.c
       plugins/main/ripple/ripple.c
       plugins/main/rotors_freq/rotors_freq.c
       plugins/main/snake_oscillo/snake_oscillo.c
       plugins/main/snake/snake.c
       src/image_filter.c
       src/image_filter.h
       src/paths.c
       src/paths.h
       src/sequence_load.c
Copyright: 1994-2021 Olivier Girondel <olivier@biniou.info>
           2019-2021 Laurent Marsac
License: GPL-2+

Files: plugins/main/fadeout_beat/fadeout_beat.c
       plugins/main/fadeout_mist/fadeout_mist.c
       plugins/main/fadeout_slow/fadeout_slow.c
       plugins/main/spheres_pulse/spheres_pulse.c
Copyright: 1994-2021 Olivier Girondel <olivier@biniou.info>
           2014-2021 Frantz Balinski
           2019-2021 Laurent Marsac
License: GPL-2+

License: GPL-2+
 This package is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program. If not, see <http://www.gnu.org/licenses/>
 .
 On Debian systems, the complete text of the GNU General
 Public License version 2 can be found in "/usr/share/common-licenses/GPL-2".

License: MIT
 Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the “Software”), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 .
 The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

License: MIT-variant
 Permission to use, copy, modify, and distribute this software and its
 documentation for any purpose and without fee is hereby granted,
 provided that the above copyright notice appear in all copies and that
 both that copyright notice and this permission notice appear in
 supporting documentation.
 .
 This file is provided AS IS with no warranties of any kind.  The author
 shall have no liability with respect to the infringement of copyrights,
 trade secrets or any patents by this file or any part thereof.  In no
 event will the author be liable for any lost revenue or profits or
 other special, indirect and consequential damages.
