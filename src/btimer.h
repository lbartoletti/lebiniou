/*
 *  Copyright 1994-2021 Olivier Girondel
 *
 *  This file is part of lebiniou.
 *
 *  lebiniou is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  lebiniou is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with lebiniou. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __TIMER_H
#define __TIMER_H

#include "utils.h"

enum TimerMode { BT_SOFT = 0, BT_HARD };

typedef struct Timer_s {
#ifdef DEBUG_TIMERS
  const char *name;
#endif
  struct timeval h_start; // hard timers
  uint64_t       s_start; // soft timers
} Timer_t;


Timer_t *Timer_new(const char *); // name will be displayed when compiled with DEBUG_TIMERS
void Timer_delete(Timer_t *);

void Timer_start(Timer_t *);
float Timer_elapsed(const Timer_t *);

void Timer_set_mode(const enum TimerMode);

#endif /* __TIMER_H */
