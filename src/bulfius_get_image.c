/*
 *  Copyright 1994-2021 Olivier Girondel
 *
 *  This file is part of lebiniou.
 *
 *  lebiniou is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  lebiniou is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with lebiniou. If not, see <http://www.gnu.org/licenses/>.
 */

#include "bulfius.h"
#include "context.h"
#include "images.h"


int
callback_get_image(const struct _u_request *request, struct _u_response *response, void *user_data)
{
  const Context_t *ctx = user_data;
  uint8_t *png = NULL;
  uint32_t png_datalen;
  const char *name = u_map_get(request->map_url, "name");
  const char *idx = u_map_get(request->map_url, "idx");

  if (NULL == images) {
    return U_CALLBACK_ERROR;
  }

  assert(NULL != ctx);

  if (NULL != name) {
    // With "name" as a parameter
    Image8_to_PNG(images->imgs[Images_name_to_index(name)], &png, &png_datalen);
  } else if (NULL != idx) {
    // By index
    uint16_t i = xstrtol(idx);
    const char *json = u_map_get(request->map_url, "json");

    if (i < images->size) {
      if (NULL != json) {
        if (is_equal(json, "true")) {
          json_t *resp = json_pack("{ssss}", "name", images->imgs[i]->name, "path", g_strdup_printf("/image?idx=%d", i));
          ulfius_set_json_body_response(response, 200, resp);
          json_decref(resp);
          goto reply;
        } else {
          return U_CALLBACK_ERROR;
        }
      } else {
        Image8_to_PNG(images->imgs[i], &png, &png_datalen);
      }
    } else {
      return U_CALLBACK_ERROR;
    }
  } else {
    // Current image
    Image8_to_PNG(ctx->imgf->dst, &png, &png_datalen);
  }
  ulfius_set_binary_body_response(response, 200, (const char *)png, png_datalen);
  xfree(png);
  ulfius_add_header_to_response(response, "Content-Type", "image/png");
  reply:
  ulfius_add_header_to_response(response, "Access-Control-Allow-Origin", "*");

  return U_CALLBACK_COMPLETE;
}
