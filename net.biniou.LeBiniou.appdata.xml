<?xml version="1.0" encoding="UTF-8"?>
<component type="desktop-application">
  <id>net.biniou.LeBiniou</id>
    <name>Le Biniou</name>
  <summary>User-friendly, powerful music visualization / VJing tool</summary>
  <metadata_license>MIT</metadata_license>
  <project_license>GPL-2.0+</project_license>
  <url type="homepage">https://biniou.net</url>
  <url type="bugtracker">https://gitlab.com/lebiniou/lebiniou/-/issues</url>
  <url type="donation">https://biniou.net/#donate</url>
  <url type="contact">https://biniou.net/contact.php</url>
  <developer_name>Olivier Girondel</developer_name>
  <update_contact>olivier_AT_biniou.net</update_contact>
  <description>
    <p>
      Le Biniou works with music, voice, ambient sounds, whatever acoustic source you choose.
    </p>
    <p>
      When you run Le Biniou it gives an evolutionary rendering of the sound you are playing.
    </p>
    <p>
      You are given two options to run Le Biniou: You can manage entirely the sequences and choose your own series of pictures from the default library, your colour scales, the kind of alteration you want to apply or you can let Le Biniou&apos;s artificial intelligence run on its own.
    </p>
    <p>
      Forget the old visualizations you are familiar with, discover a new multidimensional – spatial and chromatic – way of comprehending music and sounds for either artistic, recreational or didactic purposes.
    </p>
  </description>
  <launchable type="desktop-id">net.biniou.LeBiniou.desktop</launchable>
  <screenshots>
    <screenshot>
      <image>https://dl.biniou.net/net.biniou.LeBiniou/screenshot_20210802_201005.png</image>
    </screenshot>
    <screenshot>
      <image>https://dl.biniou.net/net.biniou.LeBiniou/screenshot_20210802_201101.png</image>
    </screenshot>
    <screenshot>
      <image>https://dl.biniou.net/net.biniou.LeBiniou/06.jpg</image>
    </screenshot>
    <screenshot>
      <image>https://dl.biniou.net/net.biniou.LeBiniou/46.jpg</image>
    </screenshot>
    <screenshot>
      <image>https://dl.biniou.net/net.biniou.LeBiniou/07.jpg</image>
    </screenshot>
    <screenshot>
      <image>https://dl.biniou.net/net.biniou.LeBiniou/08.jpg</image>
    </screenshot>
    <screenshot>
      <image>https://dl.biniou.net/net.biniou.LeBiniou/10.jpg</image>
    </screenshot>
    <screenshot>
      <image>https://dl.biniou.net/net.biniou.LeBiniou/screenshot_20210802_201322.png</image>
    </screenshot>
    <screenshot>
      <image>https://dl.biniou.net/net.biniou.LeBiniou/14.jpg</image>
    </screenshot>
    <screenshot>
      <image>https://dl.biniou.net/net.biniou.LeBiniou/15.jpg</image>
    </screenshot>
    <screenshot>
      <image>https://dl.biniou.net/net.biniou.LeBiniou/31.jpg</image>
    </screenshot>
    <screenshot>
      <image>https://dl.biniou.net/net.biniou.LeBiniou/18.jpg</image>
    </screenshot>
    <screenshot>
      <image>https://dl.biniou.net/net.biniou.LeBiniou/48.png</image>
    </screenshot>
    <screenshot>
      <image>https://dl.biniou.net/net.biniou.LeBiniou/23.jpg</image>
    </screenshot>
    <screenshot>
      <image>https://dl.biniou.net/net.biniou.LeBiniou/24.jpg</image>
    </screenshot>
    <screenshot>
      <image>https://dl.biniou.net/net.biniou.LeBiniou/screenshot_20210802_201021.png</image>
    </screenshot>
    <screenshot>
      <image>https://dl.biniou.net/net.biniou.LeBiniou/28.jpg</image>
    </screenshot>
    <screenshot>
      <image>https://dl.biniou.net/net.biniou.LeBiniou/29.jpg</image>
    </screenshot>
    <screenshot>
      <image>https://dl.biniou.net/net.biniou.LeBiniou/33.jpg</image>
    </screenshot>
    <screenshot>
      <image>https://dl.biniou.net/net.biniou.LeBiniou/37.jpg</image>
    </screenshot>
    <screenshot>
      <image>https://dl.biniou.net/net.biniou.LeBiniou/40.jpg</image>
    </screenshot>
    <screenshot>
      <image>https://dl.biniou.net/net.biniou.LeBiniou/screenshot_20210802_201033.png</image>
    </screenshot>
    <screenshot>
      <image>https://dl.biniou.net/net.biniou.LeBiniou/44.jpg</image>
    </screenshot>
    <screenshot>
      <image>https://dl.biniou.net/net.biniou.LeBiniou/screenshot_20210802_201340.png</image>
    </screenshot>
    <screenshot>
      <image>https://dl.biniou.net/net.biniou.LeBiniou/49.png</image>
    </screenshot>
  </screenshots>
  <content_rating type="oars-1.0">
    <content_attribute id="social-info">moderate</content_attribute>
  </content_rating>
  <releases>
    <release version="3.64.0" date="2021-12-31">
      <description>
        <p>You can now run several instances simultaneously. See the wiki for details.</p>
      </description>
    </release>
    <release version="3.63.4" date="2021-11-28">
      <description>
        <p>This release fixes a bug in the favorite plugins selection.</p>
      </description>
    </release>
    <release version="3.63.0" date="2021-11-01">
      <description>
        <p>Engine:</p>
        <ul>
          <li>Fixed a FTBFS on sparc64.</li>
        </ul>
        <p>Data files:</p>
        <ul>
          <li>Many images have been upscaled and converted to a 16/9 aspect ratio to look better at higher resolutions.</li>
        </ul>
        <p>Plugins:</p>
        <ul>
          <li>The OSS input plugin has been deprecated in favor of the sndio plugin.</li>
        </ul>
      </description>
    </release>
    <release version="3.61.1" date="2021-08-23">
      <description>
        <p>This release fixes compilation errors on i386/armel/armhf.</p>
      </description>
    </release>
    <release version="3.61.0" date="2021-08-02">
      <description>
        <p>Engine:</p>
        <ul>
          <li>Artificial Intelligence engine updates.</li>
        </ul>
        <p>Plugins:</p>
        <ul>
          <li>New input plugin "oscaudio" was added to receive audio using Open Sound Control.</li>
          <li>The "path" and "snake" plugins parameters were improved.</li>
          <li>Some plugins were updated for use with the new AI engine.</li>
          <li>Refactoring and bug fixes.</li>
        </ul>
        <p>Web interface:</p>
        <ul>
          <li>Display the number of connected clients.</li>
          <li>Add the possibility to select a webcam, and lock it.</li>
          <li>Fixed a bug where colormaps/images shortcuts would not store anything.</li>
        </ul>
        <p>Many thanks to the team for their contributions making this release possible !</p>
      </description>
    </release>
    <release version="3.60.1" date="2021-07-02">
      <description>
        <p>This is a major release, focused on better user experience featuring a complete rewrite of the user interface.</p>
        <p>Engine:</p>
        <ul>
          <li>Fixed locking issues which resulted in lags in the web interface. Behavior is much more fluid now.</li>
          <li>Artificial Intelligence module has been improved.</li>
          <li>MacOS is now officially supported.</li>
          <li>Fixed a bug when migrating ancient settings.</li>
          <li>Bug fixes as usual.</li>
        </ul>
        <p>Plugins:</p>
        <ul>
          <li>New output plugin "v4l2loopback" was added to stream to e.g. Open Broadcaster Software.</li>
          <li>The "mp4" output plugin has new parameters to control quality of audio/video encoding.</li>
          <li>Plugin categories have been simplified and some plugins renamed for clarity.</li>
          <li>The "twip" input plugin has been fixed.</li>
          <li>Parameters issues with the "path" plugins were corrected.</li>
          <li>"rotor" plugins parameters updates.</li>
        </ul>
        <p>Web interface:</p>
        <ul>
          <li>Rewritten using Vue3 and TypeScript.</li>
          <li>A documentation screen was added which includes a comprehensive tutorial.</li>
          <li>Option to lock the current colormap, image, or webcam.</li>
          <li>Many other improvements.</li>
        </ul>
        <p>Many thanks to the team for their contributions making this release possible !</p>
      </description>
    </release>
    <release version="3.56.1" date="2021-05-02"></release>
    <release version="3.56.0" date="2021-05-01">
      <description>
        <p>Engine:</p>
        <ul>
          <li>New layer mode "bandpass" which allows you to filter pixels by range</li>
          <li>Faster webcam rescaling</li>
          <li>Many improvements to the AI engine</li>
          <li>Enhanced websockets messages</li>
        </ul>
        <p>Plugins:</p>
        <ul>
          <li>Optimizations in the 'monitor' plugin</li>
        </ul>
        <p>Web interface:</p>
        <ul>
          <li>Added a preview of the current sequence</li>
        </ul>
      </description>
    </release>
    <release version="3.55.0" date="2021-02-27">
      <description>
        <p>Web interface additions:</p>
        <ul>
          <li>Colormap picker</li>
          <li>Image picker</li>
          <li>Plugins browser</li>
          <li>Option to add plugins to favorites, new 'favorites' tab in the home page</li>
        </ul>
      </description>
    </release>
    <release version="3.54.1" date="2021-02-16"></release>
    <release version="3.54.0" date="2021-02-14">
      <description>
        <p>Web interface updates:</p>
        <ul>
          <li>Settings have migrated to JSON</li>
          <li>New screen in the web interface to change them</li>
        </ul>
      </description>
    </release>
    <release version="3.53.3" date="2021-01-29"></release>
    <release version="3.53.2" date="2021-01-20"></release>
    <release version="3.53.1" date="2021-01-19"></release>
    <release version="3.53.0" date="2021-01-17"></release>
    <release version="3.52.0" date="2021-01-03"></release>
  </releases>
</component>
